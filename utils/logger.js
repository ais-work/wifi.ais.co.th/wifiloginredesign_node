'use strict';
const {createLogger, format, transports} = require('winston');
require('winston-daily-rotate-file');
const fs = require('fs');
const systemConstant = require('../utils/systemConstant')
const { timestamp,combine, printf } = format;
const logDir = process.env.LOGPATH;
const date = require('date-and-time');

if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}


//+++++old log+++++++//
// const myFormat = printf(({ level, message, timestamp }) => {
//     return `TIMESTAMP|${timestamp}|LOGTYPE|${level.toString().toUpperCase()}${message}`;
//   })


//   function create(filename) {
//     return createLogger({
//         format: combine(
//           timestamp({format: 'YYYY-MM-DD HH:mm:ss:SS'}),
//           myFormat
//         ),
//         transports: [

//             new transports.DailyRotateFile({
//                 filename: logDir+'/'+filename+'.%DATE%',
//                 datePattern: 'YYYYMMDDHH00',
//                 zippedArchive: true,
//                 maxFiles: '90d'
//               })
//         ]
//       });
// }




// exports.serviceInfo = function (service,obj) {
//     var logger = create('servicesLog');
//     var str = "|CALL_SERVICE|" + service;
   
//     for (var k in obj) {
      
//         str += "|" + k.toString().toUpperCase() + "|" + obj[k];
//     }

//     logger.info(str, function () {
//         logger.close();
//     });
// }
// exports.serviceError = function (service, error) {
//     var logger = create('servicesLog');
//     var str = "|CALL_SERVICE|" + service;

//     if (!systemConstant.error(error)) {
//         if (typeof error !== "undefined" && error) {
//             for (var k in error) {
//                 str += "|" + k.toString().toUpperCase() + "|" + error[k];
//             }
//         }
//     } else {
//         str += "|ERROR|" + systemConstant.error(error);
//     }

//     logger.error(str, function () {
//         logger.close();
//     });
// }

//++++++old log+++++++++//


  //+++++log+++++//
  
const format_log_header = printf(({ level, message, timestamp }) => {
    return `DATETIME|LOGTYPE|METHOD|IP|USERNAME|PASSWORD|LOGIN_SUCCESS|REPLYMESSAGE|RESPONSECODE|RESPONSEMESSAGE|ELAPSED|MACADDRESS|ALLOWTOADDMAC|UNLIMITEDACCOUNT|TIMEQUATA|REMAININGTIME|SESSIONTIME|LOGONSTATUS|SPEEDPROFILE|VLANID|CALLINGSTATIONID|IP_PORT|ERROR|UA`;
  })
const format_log = printf(({ level, message, timestamp }) => {
    return `${timestamp}|${level.toString().toUpperCase()}|${message}`;
  })

  function createlog_header(filename) {
    return createLogger({
        format: combine(
            format_log_header
        ),
        transports: [

            new transports.DailyRotateFile({
                filename: logDir+'/'+filename+'.%DATE%',
                datePattern: 'YYYYMMDD_HH',
                zippedArchive: true,
                maxFiles: '90d'
              })
        ]
      });
}

function createlog_data(filename) {
    return createLogger({
      
        format: combine(
            timestamp({format: 'YYYY-MM-DD HH:mm:ss:SS'}),
            format_log
          ),
        transports: [

            new transports.DailyRotateFile({
                filename: logDir+'/'+filename+'.%DATE%',
                datePattern: 'YYYYMMDD_HH',
                zippedArchive: true,
                maxFiles: '90d'
              })
        ]
      });
}


exports.log = function (service,obj) {

    console.log("Service" + service );
    console.log("obj" + obj );
    var filename = "log";
  
    var datefile =date.format(new Date(), "YYYYMMDD_HH");

    var path =logDir+'/'+filename+'.'+datefile;

    fs.access(path, fs.F_OK, (err) => {
        if (err) {
      
            var logger = createlog_header(filename);
            logger.info(str, function () {
                logger.close();
            });

            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k];
            }
            var logger = createlog_data(filename);
                logger.info(str, function () {
                    logger.close();
                });
    

        }else{         
            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k];
            }
            var logger = createlog_data(filename);
                logger.info(str, function () {
                    logger.close();
                });
    
        }
           
      })
}

exports.log_error = function (service,obj) {

    console.log("Service" + service );
    console.log("obj" + obj );
    var filename = "log";
  
    var datefile =date.format(new Date(), "YYYYMMDD_HH");

    var path =logDir+'/'+filename+'.'+datefile;

    fs.access(path, fs.F_OK, (err) => {
        if (err) {
      
            var logger = createlog_header(filename);
            logger.error(str, function () {
                logger.close();
            });

            
            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k];
            }
            var logger = createlog_data(filename);
                logger.info(str, function () {
                    logger.close();
                });

        }else{         
            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k]+"|";
            }
            var logger = createlog_data(filename);
                logger.error(str, function () {
                    logger.close();
                });
    
        }
           
      })
}

//++++ end log +++++//

  //++++srfp+++++//

  
  const format_srfplog_header = printf(({ level, message, timestamp }) => {
    return `DATETIME|LOGTYPE|METHOD|IP|STATUSCODE|STATUSMESSAGE|IP_PORT|PLANTEXT|PARAMS|RESPONSE|ERROR|ELAPSED|UA`;
  })
const format_srfplog = printf(({ level, message, timestamp }) => {
    return `${timestamp}|${level.toString().toUpperCase()}|${message}`;
  })

  function createsrfplog_header(filename) {
    return createLogger({
        format: combine(
            format_srfplog_header
        ),
        transports: [

            new transports.DailyRotateFile({
                filename: logDir+'/'+filename+'.%DATE%',
                datePattern: 'YYYYMMDD_HH',
                zippedArchive: true,
                maxFiles: '90d'
              })
        ]
      });
}

function createsrfplog_data(filename) {
    return createLogger({
      
        format: combine(
            timestamp({format: 'YYYY-MM-DD HH:mm:ss:SS'}),
            format_srfplog
          ),
        transports: [

            new transports.DailyRotateFile({
                filename: logDir+'/'+filename+'.%DATE%',
                datePattern: 'YYYYMMDD_HH',
                zippedArchive: true,
                maxFiles: '90d'
              })
        ]
      });
}


exports.srfplog = function (service,obj) {

    console.log("Service" + service );
    console.log("obj" + obj );
    var filename = "srfp";
  
    var datefile =date.format(new Date(), "YYYYMMDD_HH");

    var path =logDir+'/'+filename+'.'+datefile;

    fs.access(path, fs.F_OK, (err) => {
        if (err) {
      
            var logger = createsrfplog_header(filename);
            logger.info(str, function () {
                logger.close();
            });

            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k];
            }
            var logger = createsrfplog_data(filename);
                logger.info(str, function () {
                    logger.close();
                });
    

        }else{         
            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k];
            }
            var logger = createsrfplog_data(filename);
                logger.info(str, function () {
                    logger.close();
                });
    
        }
           
      })
}

exports.srfplog_error = function (service,obj) {

    console.log("Service" + service );
    console.log("obj" + obj );
    var filename = "srfp";
  
    var datefile =date.format(new Date(), "YYYYMMDD_HH");

    var path =logDir+'/'+filename+'.'+datefile;

    fs.access(path, fs.F_OK, (err) => {
        if (err) {
      
            var logger = createsrfplog_header(filename);
            logger.error(str, function () {
                logger.close();
            });

            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k];
            }
            var logger = createsrfplog_data(filename);
                logger.info(str, function () {
                    logger.close();
                });
    

        }else{         
            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k]+"|";
            }
            var logger = createsrfplog_data(filename);
                logger.error(str, function () {
                    logger.close();
                });
    
        }
           
      })
}

//++++++end srfp+++++++//


  //++++ register +++++//

  
  const format_regislog_header = printf(({ level, message, timestamp }) => {
    return `DATETIME|LOGTYPE|METHOD|IP|STATUSCODE|STATUSMESSAGE|PARAMS|IP_PORT|RESPONSE|ELAPSED|ERROR|UA`;
  })
const format_regislog = printf(({ level, message, timestamp }) => {
    return `${timestamp}|${level.toString().toUpperCase()}|${message}`;
  })

  function createregislog_header(filename) {
    return createLogger({
        format: combine(
            format_regislog_header
        ),
        transports: [

            new transports.DailyRotateFile({
                filename: logDir+'/'+filename+'.%DATE%',
                datePattern: 'YYYYMMDD_HH',
                zippedArchive: true,
                maxFiles: '90d'
              })
        ]
      });
}

function createregislog_data(filename) {
    return createLogger({
      
        format: combine(
            timestamp({format: 'YYYY-MM-DD HH:mm:ss:SS'}),
            format_regislog
          ),
        transports: [

            new transports.DailyRotateFile({
                filename: logDir+'/'+filename+'.%DATE%',
                datePattern: 'YYYYMMDD_HH',
                zippedArchive: true,
                maxFiles: '90d'
              })
        ]
      });
}


exports.registerlog = function (service,obj) {

    console.log("Service" + service );
    console.log("obj" + obj );
    var filename = "register";
  
    var datefile =date.format(new Date(), "YYYYMMDD_HH");

    var path =logDir+'/'+filename+'.'+datefile;

    fs.access(path, fs.F_OK, (err) => {
        if (err) {
      
            var logger = createregislog_header(filename);
            logger.info(str, function () {
                logger.close();
            });

            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k];
            }
            var logger = createregislog_data(filename);
                logger.info(str, function () {
                    logger.close();
                });
    

        }else{         
            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k];
            }
            var logger = createregislog_data(filename);
                logger.info(str, function () {
                    logger.close();
                });
    
        }
           
      })
}

exports.registerlog_error = function (service,obj) {

    console.log("Service" + service );
    console.log("obj" + obj );
    var filename = "register";
  
    var datefile =date.format(new Date(), "YYYYMMDD_HH");

    var path =logDir+'/'+filename+'.'+datefile;

    fs.access(path, fs.F_OK, (err) => {
        if (err) {
      
            var logger = createregislog_header(filename);
            logger.error(str, function () {
                logger.close();
            });

            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k];
            }
            var logger = createregislog_data(filename);
                logger.info(str, function () {
                    logger.close();
                });
    

        }else{         
            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k]+"|";
            }
            var logger = createregislog_data(filename);
                logger.error(str, function () {
                    logger.close();
                });
    
        }
           
      })
}

//++++++end register+++++++//


  //++++ buypack +++++//

  
  const format_buypacklog_header = printf(({ level, message, timestamp }) => {
    return `DATETIME|LOGTYPE|METHOD|IP|STATUSCODE|STATUSMESSAGE|PARAMS|IP_PORT|RESPONSE|ELAPSED|ERROR|UA`;
  })
const format_buypacklog = printf(({ level, message, timestamp }) => {
    return `${timestamp}|${level.toString().toUpperCase()}|${message}`;
  })

  function createbuypacklog_header(filename) {
    return createLogger({
        format: combine(
            format_buypacklog_header
        ),
        transports: [

            new transports.DailyRotateFile({
                filename: logDir+'/'+filename+'.%DATE%',
                datePattern: 'YYYYMMDD_HH',
                zippedArchive: true,
                maxFiles: '90d'
              })
        ]
      });
}

function createbuypacklog_data(filename) {
    return createLogger({
      
        format: combine(
            timestamp({format: 'YYYY-MM-DD HH:mm:ss:SS'}),
            format_buypacklog
          ),
        transports: [

            new transports.DailyRotateFile({
                filename: logDir+'/'+filename+'.%DATE%',
                datePattern: 'YYYYMMDD_HH',
                zippedArchive: true,
                maxFiles: '90d'
              })
        ]
      });
}


exports.buypacklog = function (service,obj) {

    console.log("Service" + service );
    console.log("obj" + obj );
    var filename = "buypack";
  
    var datefile =date.format(new Date(), "YYYYMMDD_HH");

    var path =logDir+'/'+filename+'.'+datefile;

    fs.access(path, fs.F_OK, (err) => {
        if (err) {
      
            var logger = createbuypacklog_header(filename);
            logger.info(str, function () {
                logger.close();
            });

            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k];
            }
            var logger = createbuypacklog_data(filename);
                logger.info(str, function () {
                    logger.close();
                });
    

        }else{         
            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k];
            }
            var logger = createbuypacklog_data(filename);
                logger.info(str, function () {
                    logger.close();
                });
    
        }
           
      })
}

exports.buypacklog_error = function (service,obj) {

    console.log("Service" + service );
    console.log("obj" + obj );
    var filename = "buypack";
  
    var datefile =date.format(new Date(), "YYYYMMDD_HH");

    var path =logDir+'/'+filename+'.'+datefile;

    fs.access(path, fs.F_OK, (err) => {
        if (err) {
      
            var logger = createbuypacklog_header(filename);
            logger.error(str, function () {
                logger.close();
            });

            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k];
            }
            var logger = createbuypacklog_data(filename);
                logger.info(str, function () {
                    logger.close();
                });
    

        }else{         
            var str =  service;
            for (var k in obj) {
               // console.log( obj[k]);
                str +=  "|"+obj[k]+"|";
            }
            var logger = createbuypacklog_data(filename);
                logger.error(str, function () {
                    logger.close();
                });
    
        }
           
      })
}

//++++++end buypack+++++++//

