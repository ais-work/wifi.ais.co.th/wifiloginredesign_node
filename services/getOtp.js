const rp = require('request-promise')
const systemConstant = require('../utils/systemConstant')
const common = require('../utils/common')
const logger = require('../utils/logger')
const requestIp = require('request-ip')
const { json } = require('body-parser')

exports.getOtp = function (req, res) {
    try {
        console.log('getOtp', req.body)
        const ip = requestIp.getClientIp(req);
        const clientIp = ip.substring(ip.lastIndexOf(':') + 1, ip.length)
        const startDate = new Date().getTime();
        let dataReturn
        const headers = {
            'Content-Type': 'application/json',
            'x-ssb-msisdn': common.replaceMsisdn(req.body.msisdn),
            'x-ssb-language': req.body.language,
            'x-ssb-system': req.body.eService,
            'x-ssb-client-channel': req.body.channel,
            'x-ssb-client-ip': req.headers['x-forwarded-for'] || clientIp,
            'x-ssb-session-id': '',
            "x-request-id": '',
            'x-ssb-client-browser': req.headers['user-agent']

        }
        const options = {
            host: process.env.MYAIS_SERVICES ,
            path: '/mobile/' + req.body.msisdn + '/getOtpMethod',
            qs: '',
            headers: headers,
            json: true
        };

        rp(options)
            .then(function (data) {
                dataReturn = data
                console.log("dataReturn"+JSON.stringify(dataReturn))
                logger.buypacklog('getOtpMethod', {

                    clientIp: req.headers['x-forwarded-for'] || clientIp, 
                    statuscode: '200',
                    statusmessage: 'ok',
                    params: JSON.stringify(options),              
                    url: url,
                    response: JSON.stringify(dataReturn),
                    resptime: new Date().getTime() - startDate,
                    ERROR:'',
                    UA: req.headers['user-agent'] || '',
                });
              
                res.json(dataReturn)
            })
            .catch(function (err) {
                dataReturn = {
                    isSuccess: false,
                    msg:err,
                    resultCode: 8888,

                    
                };
                console.log("dataReturn"+JSON.stringify(dataReturn))
                logger.buypacklog_error('getOtpMethod', {

                    clientIp: req.headers['x-forwarded-for'] || clientIp, 
                    statuscode: '500',
                    statusmessage: 'Error',
                    params: JSON.stringify(options), 
                    url: options.uri,
                    response: '',
                    resptime: new Date().getTime() - startDate,
                    ERROR: err ? err.toString().replace(/\|/g, ',') : '',
                    UA: req.headers['user-agent'] || '',
                });
                
                res.json(dataReturn);
            });

    } catch (e) {
        console.log(e)
        dataReturn = {
            isSuccess: false,
            msg: systemConstant.MESSAGE('9999'),
            resultCode: 9999
        };


        logger.buypacklog_error('getOtpMethod', {

            clientIp: req.headers['x-forwarded-for'] || clientIp, 
            statuscode: '500',
            statusmessage: 'Error',
            params: '',
            url: '',
            response:JSON.stringify(dataReturn),
            resptime:'',
            ERROR: e ? e.toString() : '',
            UA: req.headers['user-agent'] || '',
        });

        res.json(dataReturn);
    }


}