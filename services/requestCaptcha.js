const rp = require('request-promise')
const systemConstant = require('../utils/systemConstant')
const requestIp = require('request-ip')
const common = require('../utils/common')
const logger = require('../utils/logger')

exports.requestCaptcha = function (req, res) {
    try {
        console.log('requestCaptcha', req.body)
        const ip = requestIp.getClientIp(req);
        const clientIp = ip.substring(ip.lastIndexOf(':') + 1, ip.length)
        const startDate = new Date().getTime();
        let dataReturn
        const headers = {
            'Content-Type': 'application/json',
            "x-ssb-msisdn": common.replaceMsisdn(req.body.msisdn),
            "x-ssb-language": "",
            "x-ssb-system": "",
            "x-ssb-client-channel": "",
            "x-ssb-client-ip": req.headers['X-Forwarded-For'] || req.headers['x-forwarded-for'] || clientIp,
            "x-ssb-client-browser": req.headers['user-agent'] || req.headers['User-Agent'],
            "x-ssb-session-id": ''
        }


        const body = {
            "referenceId": req.body.referenceId
        }

        const options = {

            method: 'POST',
            uri: process.env.MYAIS_SERVICES + '/mobile/' + req.body.msisdn + '/requestOtp',
            body: body,
            headers: headers,
            json: true
        };

        rp(options)
            .then(function (data) {
                dataReturn = data

                logger.buypacklog('requestCaptcha', {

                    clientIp: req.headers['x-forwarded-for'] || clientIp, 
                    statuscode: '200',
                    statusmessage: 'ok',                
                    params: JSON.stringify(options),          
                    url: options.uri,
                    response: JSON.stringify(dataReturn),
                    resptime: new Date().getTime() - startDate,
                    ERROR:'',
                    UA: req.headers['user-agent'] || '',
                });
                res.json(dataReturn)
            })
            .catch(function (err) {
                console.log('err', err)
                dataReturn = {
                    isSuccess: false,
                    msg: systemConstant.MESSAGE('9999'),
                    resultCode: 8888
                };

                logger.buypacklog_error('requestCaptcha', {

                    clientIp: req.headers['x-forwarded-for'] || clientIp, 
                    statuscode: '500',
                    statusmessage: 'Error',
                    params: JSON.stringify(options),
                    url: options.uri,
                    response: '',
                    resptime: new Date().getTime() - startDate,
                    ERROR: err ? err.toString().replace(/\|/g, ',') : '',
                    UA: req.headers['user-agent'] || '',
                });
                res.json(dataReturn);
            });


    } catch (e) {

        console.log(e)
        dataReturn = {
            isSuccess: false,
            msg: systemConstant.MESSAGE('9999'),
            resultCode: 9999
        };


        logger.buypacklog_error('requestCaptcha', {

            clientIp: req.headers['x-forwarded-for'] || clientIp, 
            statuscode: '500',
            statusmessage: 'Error',
            params: '',
            url: '',
            response:JSON.stringify(dataReturn),
            resptime:'',
            ERROR: e ? e.toString() : '',
            UA: req.headers['user-agent'] || '',
        });

        res.json(dataReturn);
    }


}