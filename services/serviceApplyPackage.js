const rp = require('request-promise')
const systemConstant = require('../utils/systemConstant')
const requestIp = require('request-ip')
const common = require('../utils/common')
const logger = require('../utils/logger')

exports.applyPackage = function (req, res) {
    try {
        console.log('apply', req.body)
        const ip = requestIp.getClientIp(req);
        const clientIp = ip.substring(ip.lastIndexOf(':') + 1, ip.length)
        const startDate = new Date().getTime();
        const d = new Date();
        const rdnumber = d.getTime();
        let dataReturn;

        const appsettings_options = {
            uri: process.env.APP_SETTING + '/appsettings',
            qs: {
                random: rdnumber
            },
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'X-CLIENT-CHANNEL': process.env.CHANNEL,
                'X-LANGUAGE': req.body.language
            },
            json: true
        };

        rp(appsettings_options)
            .then(function (resData) {

                if (resData.apiKey) {
                    const headers = {
                        'Content-Type': 'application/json;charset=utf-8',
                        "X-SSB-MSISDN": common.replaceMsisdn(req.body.msisdn),
                        "X-SSB-Language": req.body.language,
                        "X-API-Key": resData.apiKey
                    }
                    const body = req.body.packages
                    const options = {
                        method: 'POST',
                        uri: process.env.MYAIS_SERVICES + '/mobile/' + req.body.msisdn + '/commonApplyPackages',
                        body: body,
                        headers: headers,
                        json: true
                    };
                    rp(options)
                        .then(function (data) {
                            dataReturn = data
     
                            logger.buypacklog('commonApplyPackages', {

                                clientIp: req.headers['x-forwarded-for'] || clientIp, 
                                statuscode: '200',
                                statusmessage: 'ok',
                                params: JSON.stringify(options),     
                                url: options.uri,
                                response: JSON.stringify(dataReturn),
                                resptime: new Date().getTime() - startDate,
                                ERROR:'',
                                UA: req.headers['user-agent'] || '',
                            });
                            res.json(dataReturn)
                        })
                        .catch(function (err) {
                            console.log('err', err)
                            dataReturn = {
                                isSuccess: false,
                                msg: systemConstant.MESSAGE('9999'),
                                resultCode: 8888
                            };                 

                            logger.buypacklog_error('commonApplyPackages', {

                                clientIp: req.headers['x-forwarded-for'] || clientIp, 
                                statuscode: '500',
                                statusmessage: 'Error',
                                params: JSON.stringify(options),
                                url: options.uri,
                                response: '',
                                resptime: new Date().getTime() - startDate,
                                ERROR: err ? err.toString() : '',
                                UA: req.headers['user-agent'] || '',
                            });
                            res.json(dataReturn);
                        })
                } else {
                    dataReturn = {
                        isSuccess: false,
                        msg: systemConstant.MESSAGE('9999'),
                        resultCode: 9999
                    };
 
                    logger.buypacklog_error('appsettings', {

                        clientIp: req.headers['x-forwarded-for'] || clientIp, 
                        statuscode: '500',
                        statusmessage: 'Error',
                        params: JSON.stringify(options),
                        url: options.uri,
                        response: 'Can not get apiKey',
                        resptime: new Date().getTime() - startDate,
                        ERROR: '',
                        UA: req.headers['user-agent'] || '',
                    });
                    res.json(dataReturn);
                }

                logger.buypacklog('appsettings', {

                    clientIp: req.headers['x-forwarded-for'] || clientIp, 
                    statuscode: '200',
                    statusmessage: 'ok',
                    params: JSON.stringify(options),   
                    url: options.uri,
                    response: JSON.stringify(dataReturn),
                    resptime: new Date().getTime() - startDate,
                    ERROR:'',
                    UA: req.headers['user-agent'] || '',
                });
            })
            .catch(function (appsettings_error) {
                console.log('err', appsettings_error)
                dataReturn = {
                    isSuccess: false,
                    msg: systemConstant.MESSAGE('9999'),
                    resultCode: 8888
                };

                logger.buypacklog_error('appsettings', {

                    clientIp: req.headers['x-forwarded-for'] || clientIp, 
                    statuscode: '500',
                    statusmessage: 'Error',
                    params: JSON.stringify(options),
                    url: options.uri,
                    response: appsettings_error ? appsettings_error.toString(): '',
                    resptime: new Date().getTime() - startDate,
                    ERROR: '',
                    UA: req.headers['user-agent'] || '',
                });
                res.json(dataReturn);
            })

    } catch (e) {
        console.log(e)
        dataReturn = {
            isSuccess: false,
            msg: systemConstant.MESSAGE('9999'),
            resultCode: 9999
        };

        logger.buypacklog_error('applyPackage', {

            clientIp: req.headers['x-forwarded-for'] || clientIp, 
            statuscode: '500',
            statusmessage: 'Error',
            params: '',
            url: '',
            response:JSON.stringify(dataReturn),
            resptime:'',
            ERROR: e ? e.toString() : '',
            UA: req.headers['user-agent'] || '',
        });
        res.json(dataReturn);
    }


}
exports.sendOTP = function (req, res) {
    try {
        console.log('sendOTP', req.body)
        const ip = requestIp.getClientIp(req);
        const clientIp = ip.substring(ip.lastIndexOf(':') + 1, ip.length)
        const startDate = new Date().getTime();
        let dataReturn
        const headers = {
            'Content-Type': 'application/json',
            'x-ssb-msisdn': common.replaceMsisdn(req.body.msisdn),
            'x-ssb-language': req.body.language,
            'x-ssb-system': req.body.eService,
            'x-ssb-client-channel': req.body.channel,
            'x-ssb-client-ip': req.headers['x-forwarded-for'] || clientIp,
            'x-ssb-session-id': '',
            "x-request-id": '',
            'x-ssb-client-browser': req.headers['user-agent']

        }
        const body = {
            "number": req.body.msisdn,
            "referenceId": req.body.referenceId,
            "method": "primary",
            "captcha": req.body.captcha
        }
        var options = {
            method: 'POST',
            uri: process.env.MYAIS_SERVICES + '/mobile/' + req.body.msisdn + '/sendOtp',
            body: body,
            headers: headers,
            json: true

        };

        rp(options)
            .then(function (data) {
                dataReturn = data

                logger.buypacklog('sendOtp', {

                    clientIp: req.headers['x-forwarded-for'] || clientIp, 
                    statuscode: '200',
                    statusmessage: 'ok',
                    params: JSON.stringify(options),    
                    url: options.uri,
                    response: JSON.stringify(dataReturn),
                    resptime: new Date().getTime() - startDate,
                    ERROR:'',
                    UA: req.headers['user-agent'] || '',
                });
                res.json(dataReturn)
            })
            .catch(function (err) {
                console.log('err', err)
                dataReturn = {
                    isSuccess: false,
                    msg: systemConstant.MESSAGE('9999'),
                    resultCode: 8888
                };


                logger.buypacklog_error('sendOtp', {

                    clientIp: req.headers['x-forwarded-for'] || clientIp, 
                    statuscode: '500',
                    statusmessage: 'Error',
                    params: JSON.stringify(options),
                    url: options.uri,
                    response:'',
                    resptime: new Date().getTime() - startDate,
                    ERROR:  err ? err.toString(): '',
                    UA: req.headers['user-agent'] || '',
                });
                res.json(dataReturn);
            });

    } catch (e) {
        console.log(e)
        dataReturn = {
            isSuccess: false,
            msg: systemConstant.MESSAGE('9999'),
            resultCode: 9999
        };

        logger.buypacklog_error('sendOtp', {

            clientIp: req.headers['x-forwarded-for'] || clientIp, 
            statuscode: '500',
            statusmessage: 'Error',
            params: '',
            url: '',
            response:JSON.stringify(dataReturn),
            resptime:'',
            ERROR: e ? e.toString() : '',
            UA: req.headers['user-agent'] || '',
        });
        res.json(dataReturn);
    }

}