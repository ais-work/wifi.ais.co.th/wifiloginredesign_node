const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const fs = require('fs');
const mime = require('mime-types');
const cors = require('cors');
var cons = require('consolidate');
const session = require('express-session');
const compression = require('compression')
const index = require('./controllers/controller');
const services = require('./services/serviceApplyPackage');
const getOtpMethod = require('./services/getOtp');
const requestOtp = require('./services/requestOtp');
const requestCaptcha = require('./services/requestCaptcha');
const requestOtpWithCaptcha = require('./services/requestOtpWithCaptcha');
const loginWithOtp = require('./services/loginWithOtp');
const constants = require('./constants');
const helmet = require('helmet')

let port = constants.PORT;
let pathFiles = constants.PATHFILES;

const app = express();
// view engine setup
const PORT = port || 3000;

// app.use(session({
//   key: 'username',
//   secret: "aissuperwifi",
//   resave: false,
//   saveUninitialized: false,
//   cookie: {
//     expires: 600000
//   } // expires 1 hour
// }));

app.use(compression());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(cors({
  origin: true,
  credentials: true
}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(helmet.frameguard({ action: 'SAMEORIGIN' }));
//app.use(helmet.noSniff());
//app.use(helmet.contentSecurityPolicy());

app.use((req, res, next) => {
  // respond with html page
  res.setHeader('Access-Control-Allow-Origin', 'http://delivery.adnuntius.com');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  res.setHeader('Cache-Control', 'public, max-age=31557600');
  //res.setHeader('X-Content-Type-Options' , 'nosniff');
  //res.setHeader('Content-Security-Policy' , "default-src 'self'");
  res.setHeader('X-XSS-Protection' , '1; mode=block');
  res.setHeader('Strict-Transport-Security','max-age=63072000; includeSubdomains; preload');
  res.removeHeader("X-Powered-By");
  next();

})
// res.setHeader('Access-Control-Allow-Headers', 'content-type , x-access-token');  
// res.setHeader('Access-Control-Allow-Origin', '*');

app.engine('html', cons.swig)
app.set('view engine', 'html');
app.get('/', index.renderLogin)
app.get('/logout', index.renderLogin)
app.get('/confirmlogin', index.renderLogin)
app.get('/termcondition', index.renderLogin)
app.get('/confirmregister', index.renderLogin)
app.get('/buypackage', index.renderLogin)

app.get('/vdo', index.renderLogin)
app.get('/register', index.renderLogin)
app.get('/registerppcard' , index.renderLogin)
app.get('/termconditionppcard' , index.renderLogin)
app.get('/termconditionbuypack' , index.renderLogin)
app.post('/register', index.register)
app.post('/login', index.Accountlogon)
app.post('/logout', index.Accountlogout)
app.post('/checkStatusLogon', index.CheckStatusLogon)
app.post('/getOtpMethod', getOtpMethod.getOtp)
app.post('/requestOtp', requestOtp.requestOtp)
app.post('/loginWithOtp', loginWithOtp.loginWithOtp)
app.post('/requestCaptcha', requestCaptcha.requestCaptcha)
app.post('/applyPackage', services.applyPackage)
app.post('/requestOtpWithCaptcha', requestOtpWithCaptcha.requestOtpWithCaptcha)
app.post('/sendOTP', services.sendOTP)
app.post('/registerppcard' , index.registerppcard)
app.post('/getJSONsetting' , index.getJSONsetting)
app.post('/getJSONexpiretimelist' , index.getJSONexpiretimelist)
app.post('/getAdsDomain' , index.getAdsDomain)
app.get('/-', index.renderLogin)
app.get('//gllto2.glpals.com/4day/v4/latest/lto2.dat%3f', index.renderLogin)
app.get('/*', (req, res, next) => {
  const fileToLoad = fs.readFileSync(pathFiles + req.path);
  const mimeType = mime.contentType(fileToLoad);
  res.writeHead(200, {
    'Content-Type': mimeType
  });
  res.end(fileToLoad, 'binary');
})


// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log("err in app.js line 120 >>>>>>>>>>>>>>>>>>>" + err)
  // render the error page
  res.status(err.status || 500);
  
  res.render('error');
});
app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});


module.exports = app;