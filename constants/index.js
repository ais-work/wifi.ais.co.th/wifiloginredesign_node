'use strict'

module.exports = {
    HOST: process.env.HOST || 'http://localhost',
    PORT: process.env.PORT || 3000,
    TIMEOUT: 20000,
    URLMCOA: process.env.URLMCOA,
    LOGPATH: process.env.LOGPATH,
    PATHFILES: process.env.PATHFILES,
    flagCallSRFP:process.env.flagCallSRFP,
    URLREGISTER: process.env.URLREGISTER,
    domainSRFP : "aiswlan.co.th",
    URLREGISTER_PPCARD:process.env.URLREGISTER_PPCARD,
    PROJECTCODE_PPCARD:process.env.PROJECTCODE_PPCARD,
    SRFP_URL : process.env.SRFP_URL,
    SRFP_KEY : process.env.SRFP_KEY,
    SRFP_CLIENTID : process.env.SRFP_CLIENTID,
    FREEWIFI_KEY : process.env.FREEWIFI_KEY

}
