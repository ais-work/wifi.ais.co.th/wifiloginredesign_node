const fs = require('fs');  
const date = require('date-and-time');
const soapRequest = require('easy-soap-request');
const constants = require('../constants');
var Cipher = require('aes-ecb');
const rp = require('request-promise');
// const systemConstant = require('../utils/systemConstant')
const logger = require('../utils/logger')
const requestIp = require('request-ip')

exports.InsertLog = function(header,row,filename){ 
     
    var nowDate =date.format(new Date(), "YYYY/MM/DD HH:mm:ss");
    var datefile =date.format(new Date(), "YYYYMMDD_HH");
    var path = constants.LOGPATH+ filename+datefile+".txt";
    var headerStr = "Datetime|";
    var rowStr = nowDate+"|";
    header.forEach(elementH => {
        
        headerStr += elementH + "|";
    });

    row.forEach(elementR => {
        console.log(elementR);
        rowStr += elementR + "|";
    });
    if(!fs.existsSync(path)){
           var stream = fs.createWriteStream(path);
           stream.once('open', function(fd) {
                stream.write(headerStr+"\r\n");
                stream.write(rowStr+"\r\n");
                stream.end();
              });
              stream.once('open', function(fd) {
                
                stream.end();
              });
    }else{

        fs.appendFile(path, rowStr+"\r\n", function (err) {
            if (err) {
              // append failed
            } else {
             console.log("log success");
            }
          })

    }
    
};

exports.callsoapservice= async function(url,xml,sampleHeaders) {
    try{

    
    const { response } = await soapRequest({ url: url, headers: sampleHeaders, xml: xml, timeout: 50000 }); // Optional timeout parameter(milliseconds)
        const { headers, body, statusCode } = response;
        // console.log(headers);
        // console.log(body);   
        // console.log(statusCode);    
        return  { statusCode, body };
    }catch(ex){
        console.log("callsoapservice  " + ex);
        throw new Error(ex);
    }
    
  
 }

 
exports.callSRFP = async function (reqParams) {
 
    const startDate = new Date().getTime();

   let dataReturn;
    try {
        

        var keyString = constants.SRFP_KEY;
        // var input = "{\"username\": \"66933516410\" , \"password\" : \"3182\"}";
        var parseToJsonString = {username: "66"+reqParams.username.substr(1,reqParams.username.length), password: reqParams.password}
        var input = JSON.stringify(parseToJsonString)
        var clientId = constants.SRFP_CLIENTID;
        var ip_port = constants.SRFP_URL;
        var encrypt = Cipher.encrypt(keyString, input);
        var decrypt = Cipher.decrypt(keyString,encrypt);

        var encode = encodeURIComponent(encrypt);
        var urlsrfp = ip_port+clientId+"?callbackToken="+encode;
        console.log("urlsrfp : " + urlsrfp);
        const headers = {
            'Content-Type': 'application/json'

        }
        const options = {
            uri: urlsrfp,
            qs: '',
            headers: headers,
            json: true,
            insecure: true,
            rejectUnauthorized: false,
            strictSSL: false
        };

      await rp(options)
            .then(function (data) {
         
                console.log("appcode data success >>" + JSON.stringify(data));
               
                dataReturn = {
                    isSuccess: true,
                    msg: data,
                    code: 0000
                }
                logger.srfplog('callSRFP', {
                   
                    clientIp: reqParams.clientIp,     
                    statuscode: '200',
                    statusmessage: 'OK',
                    url: options.uri,
                    plantext:JSON.stringify(parseToJsonString),
                    response:JSON.stringify(dataReturn),
                    error:"",
                    resptime: new Date().getTime() - startDate ,
                    UA: reqParams.ua,
                });

                dataReturn;
            
            })
            .catch(function (err) {
                console.log("data err     +++" + err);
                dataReturn = {
                    isSuccess: false,
                    msg: err,
                    code: 8888
                };
                logger.srfplog_error('callSRFP', {
                 
                    clientIp: reqParams.clientIp,
                    statuscode: '500',
                    statusmessage: 'Error',
                    url: options.uri,
                    plantext:JSON.stringify(parseToJsonString),
                    response: JSON.stringify(dataReturn),
                    error:err,
                    resptime: new Date().getTime() - startDate,
                    UA: reqParams.ua
                });

                 dataReturn;
        
            });

    } catch (e) {
        console.log("cathc" +e)
        dataReturn = {
            isSuccess: false,
            msg: e,
            code: 9999
        };
        

        logger.srfplog_error('callSRFP', {
           
            clientIp: reqParams.clientIp,
            statuscode: '500',
            statusmessage: 'Error',
            url: options.uri,
            plantext:JSON.stringify(parseToJsonString),
            response: JSON.stringify(dataReturn),
            error:e.toString(),
            resptime: new Date().getTime() - startDate,
            UA: reqParams.ua
        });

         dataReturn;
      console.log("datareturn" + dataReturn);
    }
    return dataReturn;

   
}



