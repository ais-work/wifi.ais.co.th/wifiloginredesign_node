exports.MESSAGE = function(code){
	switch( code ) {
		case '000':
		case '2000':
		case '20000':
			return 'ทำรายการสำเร็จ'
			break;
		case '003':
		case '2017':
			return 'คุณระบุรหัสผ่านไม่ถูกต้องเกิน 3 ครั้ง กรุณาระบุหมายเลขโทรศัพท์เคลื่อนที่ เพื่อขอรหัสผ่านใหม่อีกครั้ง'
			break;
		case '007':
		case '2014':
			return 'รหัสผ่านไม่ถูกต้อง กรุณาระบุใหม่อีกครั้ง'
			break;
		case '008':
		case '2015':
			return 'รหัสผ่านของคุณถูกขอมาเกินระยะเวลา 15 นาที กรุณาขอรหัสผ่านใหม่อีกครั้ง'
			break;
		case '020':
		case '2009':
			return 'บริการนี้สำหรับลูกค้า AIS เท่านั้น กรุณาระบุหมายเลขใหม่อีกครั้ง'
			break;
		case '025':
		case '2018':
			return 'ระบบไม่สามารถส่ง SMS ได้'
			break;
		default:
			return 'ระบบไม่สามารถทำรายการได้ในขณะนี้'
	}
}

exports.error = function( code ){
	switch( code ) {
		case '999':
			return 'Authentication error.'
			break;
		default:
			return false;
	}
}
