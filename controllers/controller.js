const fs = require('fs');
var appcode = require('./appcode.server.controller.js');
var parseString = require('xml2js').parseString;
var path = require('path');
const swig = require('swig');
const constants = require('../constants');
const session = require('express-session');
const http = require('http');
const request = require('request');
var Cipher = require('aes-ecb');
const systemConstant = require('../utils/systemConstant')
const logger = require('../utils/logger')
const requestIp = require('request-ip');
const e = require('express');
var ipRangeCheck = require("ip-range-check");
const { decrypt } = require('aes-ecb');


let pathFiles = constants.PATHFILES;
exports.renderLogin = function (req, res) {
    const ip = requestIp.getClientIp(req);
    const clientIp = ip.substring(ip.lastIndexOf(':') + 1, ip.length)
    const startDate = new Date().getTime();

    swig.renderFile('./views/WiFiSetting/index.html', {}, function (err, output) {
        if (err) {
          
             throw err;
        }

        res.send(output)
    });


}

exports.rendervdo = function (req, res) {
 

    swig.renderFile('./views/vdo.html', {}, function (err, output) {
        if (err) {
          
             throw err;
        }

        res.send(output)
    });


}

exports.rendererror = function (req, res) {
    const ip = requestIp.getClientIp(req);
    const clientIp = ip.substring(ip.lastIndexOf(':') + 1, ip.length)
    const startDate = new Date().getTime();

    swig.renderFile('./views/WiFiSetting/error.html', {}, function (err, output) {
        if (err) {
          
             throw err;
        }

        res.send(output)
    });


}

exports.getAdsDomain = function (req,res){
    const configdomain = fs.readFileSync('./adsdomain.json');
    const setIP = JSON.parse(configdomain);

    //------------------get client IP----------------------//
    const ip = requestIp.getClientIp(req);
    const clientIp = ip.substring(ip.lastIndexOf(':') + 1, ip.length)
    var ipUse = req.headers['x-forwarded-for'] || clientIp;
    
    console.log("ip use = " + ipUse);
    //---------------End--get client IP----------------------//
    var param = "default";
    var flag = false;
    for (var i = 0; i < Object.keys(setIP.ipconfig).length; i++) {
        console.log("domain.json => location >> " + setIP.ipconfig[i].location);
        console.log("domain.json => domain >> " + setIP.ipconfig[i].domain);
        console.log("domain.json => domain length >> " +Object.keys(setIP.ipconfig[i].domain).length)
        for (var j = 0; j < Object.keys(setIP.ipconfig[i].domain).length; j++) {
            console.log("adsdomain.json => adsdomain ip >> " + setIP.ipconfig[i].domain[j]);
           //const ipv4CIDR = getIPRange(setIP.ipconfig[i].domain[j]);
            //console.log("ipvCIDR => " + ipv4CIDR);
            var checkip = ipRangeCheck(ipUse, setIP.ipconfig[i].domain[j]);
            console.log("checkip :: "+checkip);
            if(checkip){
                param = setIP.ipconfig[i].location;
                flag = true;
                break;
            }
           
        } 
        if(flag){
            break;
        }
    } 

    console.log("param location ads >>>>>>>" +param )


    const resData = {
        param: param
        };
    console.log(JSON.stringify((resData)));
    res.json(resData);


}

exports.getJSONexpiretimelist = function (req, res) {

    const domainlist = fs.readFileSync('./expiretimelist.json');
    const domainlist_parse = JSON.parse(domainlist);
    console.log(domainlist_parse.list_domain);
    res.json(domainlist_parse.list_domain);
}

exports.getJSONsetting = function (req, res) {

    console.log("getjsonsetting in");
    const configbanner = fs.readFileSync('./setting.json');
    const setting = JSON.parse(configbanner);

    const configdomain = fs.readFileSync('./domain.json');
    const setIP = JSON.parse(configdomain);

    //------------------get client IP----------------------//
    const ip = requestIp.getClientIp(req);
    const clientIp = ip.substring(ip.lastIndexOf(':') + 1, ip.length)
    var ipUse = req.headers['x-forwarded-for'] || clientIp;
    
    console.log("ip use = " + ipUse);
    //---------------End--get client IP----------------------//
    var param = "aisportal";
    var flag = false;
    for (var i = 0; i < Object.keys(setIP.ipconfig).length; i++) {
        console.log("domain.json => location >> " + setIP.ipconfig[i].location);
        console.log("domain.json => domain >> " + setIP.ipconfig[i].domain);
        console.log("domain.json => domain length >> " +Object.keys(setIP.ipconfig[i].domain).length)
        for (var j = 0; j < Object.keys(setIP.ipconfig[i].domain).length; j++) {
            console.log("domain.json => domain ip >> " + setIP.ipconfig[i].domain[j]);
           //const ipv4CIDR = getIPRange(setIP.ipconfig[i].domain[j]);
          // console.log("ipvCIDR => " + ipv4CIDR);
            var checkip = ipRangeCheck(ipUse, setIP.ipconfig[i].domain[j]);
            console.log("checkip :: "+checkip);
            if(checkip){
                param = setIP.ipconfig[i].location;
                flag = true;
                break;
            }
           
        } 
        if(flag){
            break;
        }
    } 

    console.log("param >>>>>>>" +param )

    
    console.log("len >>>>"+Object.keys(setting.config).length);
    // for(var i;i<Object.keys(smallbanner.config).length;i++){
    //     console.log("test");
    //     //console.log(smallbanner.config[i].location);
    // }
   
    
    for (var i = 0; i < Object.keys(setting.config).length; i++) {
     console.log("test")
     console.log(setting.config[i].location);
     if(setting.config[i].location == param){
        console.log(setting.config[i].setting_page);
         res.json(setting.config[i].setting_page);
        
     }
    } 
   //var rtn = { smallbanner: vdourl.vdo_th, vdo_en: vdourl.vdo_en };
    
}



exports.Accountlogon = function (req, res) {
    const ip = requestIp.getClientIp(req);
    const clientIp = ip.substring(ip.lastIndexOf(':') + 1, ip.length)
    const startDate = new Date().getTime();
 

    try {

        console.log("req.body"+ JSON.stringify(req.body));
     //var reqbody = JSON.stringify(req.body);
     //var test = JsonConvert.DeserializeObject(req.body);
     //req.body = JSON.stringify(req.body).Replace("\\", "");
     //console.log(req.body[0].txtUsername);
 



        var username = req.body.txtUsername != null ?req.body.txtUsername :"";
        // req.session.username = username;
        var password = req.body.txtPassword!= null ?req.body.txtPassword :"";
        console.log("username pass +"+ username + password);
        if(username == "" && username == null ){

           
            const errjson = {
                responseCode: "0000",
                replyMessage:"SBR-0404"                      
            }
            console.log(JSON.stringify((errjson)));
            res.json(JSON.stringify((errjson)));
        }

        
        if(password == "" && password == null ){
            
            const errjson = {
                responseCode: "0000",
                replyMessage:"SBR-0404"                      
            }
            console.log(JSON.stringify((errjson)));
            res.json(JSON.stringify((errjson)));
        }

        var ipUse = req.headers['x-forwarded-for'] || clientIp;
        var ua = req.headers['user-agent'];
        var flagSRFP = constants.flagCallSRFP;
        console.log("flagSRFP" + flagSRFP);
        console.log("username+password+ipAddress>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + username + password + ipUse)
        if (flagSRFP == "true") {
            const reqParams = {
                username: username,
                password: password,
                clientIp: ipUse,
                ua : ua
            }
            appcode.callSRFP(reqParams).then(data => {
                var isSuccess = data.isSuccess != null ? data.isSuccess : "false";
                var resultcode = data.msg.resultCode != null ? data.msg.resultCode :"";
                console.log("resultcode +++"+resultcode);
                console.log("isSuccess +++"+isSuccess);

               if(resultcode == "20000" && isSuccess == "true"){
                    var privateID = data.msg.privateId != null ? data.msg.privateId : "";
                    var accessToken = data.msg.accessToken != null ? data.msg.accessToken : "";
  
                    username = privateID + "@" + constants.domainSRFP;
                    password = accessToken;
                    console.log("username" + username );
                    console.log("password" + password );



                                // example data
                    const url = constants.URLMCOA;
                    console.log("url>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + url)

                    const sampleHeaders = {
                        'user-agent': req.headers['user-agent'],
                        'Content-Type': 'text/xml;charset=UTF-8',
                        'soapAction': url,
                    };

                    const xml = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wifi=\"http://wifi.ais.co.th/\">" +
                        "<soap:Header/>" +
                        "<soap:Body>" +
                        "<wifi:accountLogon>" +
                        //<!--Optional:-->
                        "<username>" + username + "</username>" +
                        //<!--Optional:-->
                        "<password>" + password + "</password>" +
                        //<!--Optional:-->
                        "<ipAddress>" + ipUse + "</ipAddress>" +
                        "</wifi:accountLogon>" +
                        "</soap:Body>" +
                        "</soap:Envelope>";

                    // usage of module
                        console.log(xml);

                        const params = {
                            username: username,
                            password: password,
                            ipAddress: ipUse
                        }
                    appcode.callsoapservice(url, xml, sampleHeaders).then(data => {

                        const statusCode = data["statusCode"]
                        parseString(data["body"], function (err, result) {
                            var JOB = result["soap:Envelope"]["soap:Body"][0]["ns2:accountLogonResponse"][0]["return"][0];
                            console.log("accountlogon >>>>>>>>>>" + JSON.stringify(JOB));
                            res.json(JSON.stringify(JOB));
                           
                        
                            
                            logger.log('Accountlogon_srfp_allow', {
                                
                                clientIp: req.headers['x-forwarded-for'] || clientIp,                             
                                username:params.username+","+ JOB.userName!=null ?JOB.userName :"" ,
                                password : params.password,                             
                                login_success :JOB.login_success!=null ?JOB.login_success :"" ,
                                replymessage : JOB.replyMessage!=null ?JOB.replyMessage :"",
                                responseCode : JOB.responseCode!=null ?JOB.responseCode :"",
                                responsemessage : JOB.responseMessage!=null ?JOB.responseMessage :"",
                                resptime: new Date().getTime() - startDate,
                                macAddress:JOB.macAddress!=null ?JOB.macAddress :"",
                                allowToAddMac:JOB.allowToAddMac!=null ?JOB.allowToAddMac :"",
                                unlimitedaccount : JOB.unlimitedAccount!=null ?JOB.unlimitedAccount :"",
                                timequata:JOB.timeQuota!=null ?JOB.timeQuota :"",
                                remainingTime: JOB.remainingTime!=null ?JOB.remainingTime :"",
                                sessiontime:JOB.sessionTime!=null ?JOB.sessionTime :"",
                                logonstatus:JOB.logonStatus!=null ?JOB.logonStatus :"",
                                speedprofile:JOB.speedProfile!=null ?JOB.speedProfile :"",
                                vlanId:JOB.vlanId!=null ?JOB.vlanId :"",
                                callingStationId:JOB.callingStationId!=null ?JOB.callingStationId :"",
                                url: url,
                                error:"",
                                UA: req.headers['user-agent'] || '',
                               
                            });
                        })

                    }).catch((e) => {
                      

                       

                        const resData = {
                            isSuccess: false,
                            msg: systemConstant.MESSAGE('9999'),
                            responseCode: 9999,
                            ex: e
                        };
                        console.log(JSON.stringify((resData)))
                        res.json(JSON.stringify((resData)));
                            logger.log_error('Accountlogon_srfp_allow', {
                                
                                clientIp: req.headers['x-forwarded-for'] || clientIp,                             
                                username:params.username,
                                password : params.password,                             
                                login_success :"" ,
                                replymessage : "",
                                responseCode : "",
                                responsemessage :"",
                                resptime: new Date().getTime() - startDate,
                                macAddress:"",
                                allowToAddMac:"",
                                unlimitedaccount : "",
                                timequata:"",
                                remainingTime:"",
                                sessiontime:"",
                                logonstatus:"",
                                speedprofile:"",
                                vlanId:"",
                                callingStationId:"",
                                url: url,
                                error:e,
                                UA: req.headers['user-agent'] || '',
                               
                            });
                    });

    




               }else{
                   
                        // example data
                    const url = constants.URLMCOA;
                    console.log("url>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + url)

                    const sampleHeaders = {
                        'user-agent': req.headers['user-agent'],
                        'Content-Type': 'text/xml;charset=UTF-8',
                        'soapAction': url,
                    };

                    const xml = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wifi=\"http://wifi.ais.co.th/\">" +
                        "<soap:Header/>" +
                        "<soap:Body>" +
                        "<wifi:accountLogon>" +
                        //<!--Optional:-->
                        "<username>" + username + "</username>" +
                        //<!--Optional:-->
                        "<password>" + password + "</password>" +
                        //<!--Optional:-->
                        "<ipAddress>" + ipUse + "</ipAddress>" +
                        "</wifi:accountLogon>" +
                        "</soap:Body>" +
                        "</soap:Envelope>";

                    // usage of module
                        console.log(xml);
                        const params = {
                            username: username,
                            password: password,
                            ipAddress: ipUse
                        }
                    appcode.callsoapservice(url, xml, sampleHeaders).then(data => {

                        const statusCode = data["statusCode"]
                        parseString(data["body"], function (err, result) {
                            var JOB = result["soap:Envelope"]["soap:Body"][0]["ns2:accountLogonResponse"][0]["return"][0];
                            console.log("accountlogon >>>>>>>>>>" + JSON.stringify(JOB));
                            res.json(JSON.stringify(JOB));
                      
                            logger.log('Accountlogon_srfp_notallow', {
                                
                                clientIp: req.headers['x-forwarded-for'] || clientIp,                             
                                username:params.username+","+ JOB.userName!=null ?JOB.userName :"" ,
                                password : params.password,                             
                                login_success :JOB.login_success!=null ?JOB.login_success :"" ,
                                replymessage : JOB.replyMessage!=null ?JOB.replyMessage :"",
                                responseCode : JOB.responseCode!=null ?JOB.responseCode :"",
                                responsemessage : JOB.responseMessage!=null ?JOB.responseMessage :"",
                                resptime: new Date().getTime() - startDate,
                                macAddress:JOB.macAddress!=null ?JOB.macAddress :"",
                                allowToAddMac:JOB.allowToAddMac!=null ?JOB.allowToAddMac :"",
                                unlimitedaccount : JOB.unlimitedAccount!=null ?JOB.unlimitedAccount :"",
                                timequata:JOB.timeQuota!=null ?JOB.timeQuota :"",
                                remainingTime: JOB.remainingTime!=null ?JOB.remainingTime :"",
                                sessiontime:JOB.sessionTime!=null ?JOB.sessionTime :"",
                                logonstatus:JOB.logonStatus!=null ?JOB.logonStatus :"",
                                speedprofile:JOB.speedProfile!=null ?JOB.speedProfile :"",
                                vlanId:JOB.vlanId!=null ?JOB.vlanId :"",
                                callingStationId:JOB.callingStationId!=null ?JOB.callingStationId :"",
                                url: url,
                                error:"",
                                UA: req.headers['user-agent'] || '',
                               
                            });
                        })

                    }
                    
                    
                    ).catch((e) => {
                        
                      
                        const resData = {
                            isSuccess: false,
                            msg: systemConstant.MESSAGE('9999'),
                            responseCode: 9999,
                            ex: e
                        }; 
                        
                        console.log(JSON.stringify((resData)));
                        res.json(JSON.stringify((resData)));
                        logger.log_error('Accountlogon_srfp_notallow', {
                                
                            clientIp: req.headers['x-forwarded-for'] || clientIp,                             
                            username:params.username,
                            password : params.password,                             
                            login_success :"" ,
                            replymessage : "",
                            responseCode : "",
                            responsemessage :"",
                            resptime: new Date().getTime() - startDate,
                            macAddress:"",
                            allowToAddMac:"",
                            unlimitedaccount : "",
                            timequata:"",
                            remainingTime:"",
                            sessiontime:"",
                            logonstatus:"",
                            speedprofile:"",
                            vlanId:"",
                            callingStationId:"",
                            url: url,
                            error:e,
                            UA: req.headers['user-agent'] || '',
                           
                        });
                    });
               }


               console.log(" log success");
               logger.srfplog('resultSRFP', {
      
                clientIp: ipUse,
                statuscode: '200',
                statusmessage: 'OK',
                url: flagSRFP,
                plantext:"",
                params: JSON.stringify(reqParams),
                response:  data ? JSON.stringify(data) : '',
                error:"",
                resptime: new Date().getTime() - startDate,
                UA: ua,
            })

            },
            error => {

                console.log("resultSRFP error in");

                
                logger.serviceError('resultSRFP', {
         
                    clientIp: ipUse,
                    statuscode: '500',
                    statusmessage: 'Error',
                    url: flagSRFP,
                    plantext:"",
                    params: JSON.stringify(reqParams),
                    response:  data ? JSON.stringify(data) : '',
                    error:error ? error : '',
                    resptime: new Date().getTime() - startDate,
                    UA: ua,
                })

            }
            )

        }else{
            // srfp is false

              // example data
        const url = constants.URLMCOA;
        console.log("url>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + url)

        const sampleHeaders = {
            'user-agent': req.headers['user-agent'],
            'Content-Type': 'text/xml;charset=UTF-8',
            'soapAction': url,
        };

        const xml = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wifi=\"http://wifi.ais.co.th/\">" +
            "<soap:Header/>" +
            "<soap:Body>" +
            "<wifi:accountLogon>" +
            //<!--Optional:-->
            "<username>" + username + "</username>" +
            //<!--Optional:-->
            "<password>" + password + "</password>" +
            //<!--Optional:-->
            "<ipAddress>" + ipUse + "</ipAddress>" +
            "</wifi:accountLogon>" +
            "</soap:Body>" +
            "</soap:Envelope>";

        // usage of module
            console.log(xml);
        appcode.callsoapservice(url, xml, sampleHeaders).then(data => {

            const statusCode = data["statusCode"]
            parseString(data["body"], function (err, result) {
                var JOB = result["soap:Envelope"]["soap:Body"][0]["ns2:accountLogonResponse"][0]["return"][0];
                console.log("accountlogon >>>>>>>>>>" + JSON.stringify(JOB));
           
                   
                console.log( "logonstatus > " +  JSON.stringify(JOB["logonStatus"]) );
                var logonStatus = JSON.stringify(JOB["logonStatus"]);
                if( logonStatus.indexOf("true") != -1){
                    console.log("test in this testetst");
                }
                
                res.json(JSON.stringify(JOB));


                const params = {
                    username: username,
                    password: password,
                    ipAddress: ipUse
                }
        
                logger.log('Accountlogon', {
                                
                    clientIp: req.headers['x-forwarded-for'] || clientIp,                             
                    username:params.username+","+ JOB.userName!=null ?JOB.userName :"" ,
                    password : params.password,                             
                    login_success :JOB.login_success!=null ?JOB.login_success :"" ,
                    replymessage : JOB.replyMessage!=null ?JOB.replyMessage :"",
                    responseCode : JOB.responseCode!=null ?JOB.responseCode :"",
                    responsemessage : JOB.responseMessage!=null ?JOB.responseMessage :"",
                    resptime: new Date().getTime() - startDate,
                    macAddress:JOB.macAddress!=null ?JOB.macAddress :"",
                    allowToAddMac:JOB.allowToAddMac!=null ?JOB.allowToAddMac :"",
                    unlimitedaccount : JOB.unlimitedAccount!=null ?JOB.unlimitedAccount :"",
                    timequata:JOB.timeQuota!=null ?JOB.timeQuota :"",
                    remainingTime: JOB.remainingTime!=null ?JOB.remainingTime :"",
                    sessiontime:JOB.sessionTime!=null ?JOB.sessionTime :"",
                    logonstatus:JOB.logonStatus!=null ?JOB.logonStatus :"",
                    speedprofile:JOB.speedProfile!=null ?JOB.speedProfile :"",
                    vlanId:JOB.vlanId!=null ?JOB.vlanId :"",
                    callingStationId:JOB.callingStationId!=null ?JOB.callingStationId :"",
                    url: url,
                    error:"",
                    UA: req.headers['user-agent'] || '',
                   
                });
            })

        }).catch((e) => {

           
            const resData = {
                isSuccess: false,
                msg: systemConstant.MESSAGE('9999'),
                responseCode: 9999,
                ex: e
            };

            const params = {
                username: username,
                password: password,
                ipAddress: ipUse
            }

            console.log(JSON.stringify((resData)));
            res.json(JSON.stringify((resData)));

            logger.log_error('Accountlogon', {
                                
                clientIp: req.headers['x-forwarded-for'] || clientIp,                             
                username:params.username,
                password : params.password,                             
                login_success :"" ,
                replymessage : "",
                responseCode : "",
                responsemessage :"",
                resptime: new Date().getTime() - startDate,
                macAddress:"",
                allowToAddMac:"",
                unlimitedaccount : "",
                timequata:"",
                remainingTime:"",
                sessiontime:"",
                logonstatus:"",
                speedprofile:"",
                vlanId:"",
                callingStationId:"",
                url: url,
                error:e,
                UA: req.headers['user-agent'] || '',
               
            });
        });

    } 


    }catch (ex) {

        const params = {
            username: username,
            password: password,
            ipAddress: ipUse
        }
        logger.log_error('Accountlogon.', {
                                
            clientIp: req.headers['x-forwarded-for'] || clientIp,                             
            username:params.username,
            password : params.password,                             
            login_success :"" ,
            replymessage : "",
            responseCode : "",
            responsemessage :"",
            resptime: new Date().getTime() - startDate,
            macAddress:"",
            allowToAddMac:"",
            unlimitedaccount : "",
            timequata:"",
            remainingTime:"",
            sessiontime:"",
            logonstatus:"",
            speedprofile:"",
            vlanId:"",
            callingStationId:"",
            url: url,
            error:ex.toString(),
            UA: req.headers['user-agent'] || '',
           
        });

        const errjson = {
            responseCode: 9999,
            ex: ex.toString(),                       
        }
        console.log(JSON.stringify((errjson)));

      
        res.json(JSON.stringify((errjson)));
    

    }
}


exports.Accountlogout = function (req, res) {
    const ip = requestIp.getClientIp(req);
    const clientIp = ip.substring(ip.lastIndexOf(':') + 1, ip.length)
    const startDate = new Date().getTime();


    try {


        var ipUse = req.headers['x-forwarded-for'] || clientIp;

        // example data
        const url = constants.URLMCOA;
        console.log("url>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + url)

        const sampleHeaders = {
            'user-agent': req.headers['user-agent'],
            'Content-Type': 'text/xml;charset=UTF-8',
            'soapAction': url,
        };

        const xml = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wifi=\"http://wifi.ais.co.th/\">" +
            "<soap:Header/>" +
            "<soap:Body>" +
            "<wifi:accountLogoff>" +
            "<ipAddress>" + ipUse + "</ipAddress>" +
            "</wifi:accountLogoff>" +
            "</soap:Body>" +
            "</soap:Envelope>";

        // usage of module

        appcode.callsoapservice(url, xml, sampleHeaders).then(data => {

            const statusCode = data["statusCode"];
            parseString(data["body"], function (err, result) {

                if(err){
                     const resData = {
                    isSuccess: false,
                    msg: systemConstant.MESSAGE('9999'),
                    responseCode: 9999,
                    ex: e
                };
            
                res.json(JSON.stringify((resData)));
            
                logger.log_error('accountLogoff', {
                                
                    clientIp: req.headers['x-forwarded-for'] || clientIp,                             
                    username:params.username,
                    password : params.password,                             
                    login_success :"" ,
                    replymessage : "",
                    responseCode : "",
                    responsemessage :"",
                    resptime: new Date().getTime() - startDate,
                    macAddress:"",
                    allowToAddMac:"",
                    unlimitedaccount : "",
                    timequata:"",
                    remainingTime:"",
                    sessiontime:"",
                    logonstatus:"",
                    speedprofile:"",
                    vlanId:"",
                    callingStationId:"",
                    url: url,
                    error:e,
                    UA: req.headers['user-agent'] || '',
                   
                });
                }
                var JOB = result["soap:Envelope"]["soap:Body"][0]["ns2:accountLogoffResponse"][0]["return"][0];
                console.log("accountLogout >>>>" + JSON.stringify(JOB));
            
                res.json(JSON.stringify(JOB));
                const params = {
                    ipAddress: ipUse
                }
            

                logger.log('accountLogoff', {
                                
                    clientIp: req.headers['x-forwarded-for'] || clientIp,                             
                    username:"",
                    password : "",                             
                    login_success :JOB.login_success!=null ?JOB.login_success :"" ,
                    replymessage : JOB.replyMessage!=null ?JOB.replyMessage :"",
                    responseCode : JOB.responseCode!=null ?JOB.responseCode :"",
                    responsemessage : JOB.responseMessage!=null ?JOB.responseMessage :"",
                    resptime: new Date().getTime() - startDate,
                    macAddress:JOB.macAddress!=null ?JOB.macAddress :"",
                    allowToAddMac:JOB.allowToAddMac!=null ?JOB.allowToAddMac :"",
                    unlimitedaccount : JOB.unlimitedAccount!=null ?JOB.unlimitedAccount :"",
                    timequata:JOB.timeQuota!=null ?JOB.timeQuota :"",
                    remainingTime: JOB.remainingTime!=null ?JOB.remainingTime :"",
                    sessiontime:JOB.sessionTime!=null ?JOB.sessionTime :"",
                    logonstatus:JOB.logonStatus!=null ?JOB.logonStatus :"",
                    speedprofile:JOB.speedProfile!=null ?JOB.speedProfile :"",
                    vlanId:JOB.vlanId!=null ?JOB.vlanId :"",
                    callingStationId:JOB.callingStationId!=null ?JOB.callingStationId :"",
                    url: url,
                    error:"",
                    UA: req.headers['user-agent'] || '',
                   
                });
            })

        });


    } catch (ex) {


        logger.log_error('accountLogoff', {
                                
            clientIp: req.headers['x-forwarded-for'] || clientIp,                             
            username:"",
            password : "",                             
            login_success :"" ,
            replymessage : "",
            responseCode :"",
            responsemessage :"",
            resptime: new Date().getTime() - startDate,
            macAddress:"",
            allowToAddMac:"",
            unlimitedaccount : "",
            timequata:"",
            remainingTime:"",
            sessiontime:"",
            logonstatus:"",
            speedprofile:"",
            vlanId:"",
            callingStationId:"",
            url: url,
            error:ex.toString(),
            UA: req.headers['user-agent'] || '',
           
        });
        const errjson = {
            responseCode: 9999,
            ex: ex,                       
        }
        console.log(JSON.stringify((errjson)));
        res.json(JSON.stringify((errjson)));
    

    }
}


    exports.register = function (req, res) {
    const ip = requestIp.getClientIp(req);
    const clientIp = ip.substring(ip.lastIndexOf(':') + 1, ip.length)
    const startDate = new Date().getTime();
    
    try {
        var keyString = constants.FREEWIFI_KEY;
        //var test = "MbG9zH88EnHRZq5yTCyukixgWv5NYgCMt1RKG7CC14g%3D";

 
      
        var regis_packagecode = req.body.hiddenPackageID != null ? req.body.hiddenPackageID : "%2BQ7V0%2B3qnBcB%2Fw3%2FeMRVv8v6m8GkLYGUNhA0aA%2F5Zz4%3D";
        console.log("regis_packagecode > " + regis_packagecode );
        var decodetext = decodeURIComponent(regis_packagecode);
      // var decodetext = decodeURIComponent(test);
       console.log("decodetext > " + decodetext );
        var package = "S";
        if(regis_packagecode != ""){
            var decrypttext = Cipher.decrypt(keyString,decodetext); 
           // console.log("test > " + test );
            //console.log("keyString > " + keyString );
            
            console.log("decrypttext > " + decrypttext );
            var charpackage = decrypttext.substring(11,12)

            console.log("test >>> " + charpackage);
            if(charpackage == "9"){
                package = "S";
            }else if(charpackage == "5"){
                package = "M";
            }else if(charpackage == "2"){
                package = "L";
            }

            console.log("package >" + package);
        }

        var regis_age = req.body.ddlAge != null ? req.body.ddlAge : "";
        var regis_title = req.body.ddlTitle != null ? req.body.ddlTitle : "";
        var regis_mobileOperator = req.body.ddlOperator != null ? req.body.ddlOperator :"";
        
        var regis_email = req.body.txtEmail != null ? req.body.txtEmail : "";
        var regis_firstname = req.body.txtFirstName != null ? req.body.txtFirstName : "";
        var regis_lastname = req.body.txtLastName != null ? req.body.txtLastName : "";
        var regis_mobile = req.body.txtMobile != null ? req.body.txtMobile : "";
        
        var regis_passport = req.body.txtPassport != null ? req.body.txtPassport : "";
        var regis_language = req.body.txtLanguage != null ? req.body.txtLanguage : "";
        var regis_idcard = req.body.txtIDcard != null ? req.body.txtIDcard : "";
        var ipUse = req.headers['x-forwarded-for'] || clientIp;

        console.log(regis_firstname + regis_lastname + regis_idcard + regis_mobile +regis_packagecode + regis_age + regis_mobileOperator);
        // example data
        const url = constants.URLREGISTER;
        console.log("url>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + url)

        const sampleHeaders = {
            'user-agent': req.headers['user-agent'],
            'Content-Type': 'text/xml;charset=UTF-8',
            'soapAction': url,
        };

        const xml = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:ws=\"http://ws.wifi.ais.co.th/\">" +
            "<soap:Header/>" +
            "<soap:Body>" +
            "<ws:GenerateAccount>" +
            //<!--Optional:-->
            "<arg0>" +
            //<!--Optional:-->
            "<AGE>" + regis_age + "</AGE>" +
            //<!--Optional:-->
            "<EMAIL_ADDRESS>" + regis_email + "</EMAIL_ADDRESS>" +
            //<!--Optional:-->
            "<FIRST_NAME>" + regis_firstname + "</FIRST_NAME>" +
            //<!--Optional:-->
            "<LAST_NAME>" + regis_lastname + "</LAST_NAME>" +
            //<!--Optional:-->
            "<MOBILE_NO>" + regis_mobile + "</MOBILE_NO>" +
            //<!--Optional:-->
            "<MOBILE_OPERATOR>" + regis_mobileOperator + "</MOBILE_OPERATOR>" +
            //<!--Optional:-->
            "<PACKAGE_CODE>" + package + "</PACKAGE_CODE>" +
            //<!--Optional:-->
            "<PASSPORT_ID>" + regis_passport + "</PASSPORT_ID>" +
            //<!--Optional:-->
            "<PORTAL_LANGUAGE>" + regis_language + "</PORTAL_LANGUAGE>" +
            //<!--Optional:-->
            "<THAI_CITIZEN_ID>" + regis_idcard + "</THAI_CITIZEN_ID>" +
             //<!--Optional:-->
             "<TITLE>" + regis_title + "</TITLE>" +
            
            "</arg0>" +
            "</ws:GenerateAccount>" +
            "</soap:Body>" +
            "</soap:Envelope>";

        // usage of module
        const params = {
            EMAIL_ADDRESS: regis_email,
            FIRST_NAME: regis_firstname,
            LAST_NAME: regis_lastname,
            MOBILE_NO: regis_mobile,
            PACKAGE_CODE: regis_packagecode,
            PASSPORT_ID: regis_passport,
            PORTAL_LANGUAGE: regis_language,
            THAI_CITIZEN_ID: regis_idcard,
            IPAddress: ipUse
        }
        var result = appcode.callsoapservice(url, xml, sampleHeaders).then(data => {
     
            const statusCode = data["statusCode"]

            parseString(data["body"], function (err, result) {
                if(err){
                        
                    const resData = {
                        isSuccess: false,
                        msg: systemConstant.MESSAGE('9999'),
                        responseCode: 9999,
                        ex: err
                    };    
            
                    logger.registerlog_error('register', {
                     
                        clientIp: req.headers['x-forwarded-for'] || clientIp, 
                        statuscode: '500',
                        statusmessage: 'error',
                        params: JSON.stringify(params),                 
                        url: url,
                        response: JSON.stringify(resData),
                        resptime: new Date().getTime() - startDate,
                        ERROR:err?err.toString():'',
                        UA: req.headers['user-agent'] || '',
                    });
                    console.log(JSON.stringify((resData)));
                    res.json(JSON.stringify((resData)));
                
                }
                
                var JOB = result["soap:Envelope"]["soap:Body"][0]["ns2:GenerateAccountResponse"][0]["return"][0];
                console.log("register account >>>>" + JSON.stringify(JOB));

                var resCode = JOB.hasOwnProperty("responseCode") == true ? JOB.responseCode.toString() :"";
                console.log(resCode);
                 var resMessage = JOB.hasOwnProperty("responseMessage") == true ? JOB.responseMessage.toString():"";
                console.log(resMessage);

                var resUsername = JOB.hasOwnProperty("username") == true ? JOB.username.toString():"";
                console.log(resUsername);

                var retdata = {responseCode : resCode , responseMessage :resMessage , username : resUsername};
                res.json(JSON.stringify(retdata));
              
                logger.registerlog('register', {

                    clientIp: req.headers['x-forwarded-for'] || clientIp, 
                    statuscode: statusCode,
                    statusmessage: 'ok',
                    params: JSON.stringify(params),                 
                    url: url,
                    response: JSON.stringify(JOB),
                    resptime: new Date().getTime() - startDate,
                    ERROR:'',
                    UA: req.headers['user-agent'] || '',
                });
            }
                 
            
            )

        });


    } catch (ex) {

 
        const errjson = {
            responseCode: 9999,
            ex: ex,                       
        }
        logger.registerlog_error('register', {
                     
            clientIp: req.headers['x-forwarded-for'] || clientIp, 
            statuscode: '500',
            statusmessage: 'error',
            params: '',                 
            url: url,
            response: JSON.stringify(errjson),
            resptime: new Date().getTime() - startDate,
            ERROR:ex.toString(),
            UA: req.headers['user-agent'] || '',
        });

      
        console.log(JSON.stringify((errjson)));
        res.json(JSON.stringify((errjson)));
    

    }
}


exports.registerppcard = function (req, res) {
    const ip = requestIp.getClientIp(req);
    const clientIp = ip.substring(ip.lastIndexOf(':') + 1, ip.length)
    const startDate = new Date().getTime();

    try {


        // var username =  req.session.username;
        var regis_email = req.body.txtEmail != null ? req.body.txtEmail : "";
        var regis_firstname = req.body.txtFirstName != null ? req.body.txtFirstName : "";
        var regis_lastname = req.body.txtLastName != null ? req.body.txtLastName : "";
        var regis_mobile = req.body.txtMobile != null ? req.body.txtMobile : "";
        var regis_passport = req.body.txtPassport != null ? req.body.txtPassport : "";
        var regis_language = req.body.txtLanguage != null ? req.body.txtLanguage : "";
        var regis_idcard = req.body.txtIDcard != null ? req.body.txtIDcard : "";
        var regis_username = req.body.hiddenUsername != null ? req.body.hiddenUsername : "";
        var regis_password = req.body.hiddenPassword != null ? req.body.hiddenPassword : "";

        var ipUse = req.headers['x-forwarded-for'] || clientIp;

      
        // example data
        const url = constants.URLREGISTER_PPCARD;
        console.log("url>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + url)

        const sampleHeaders = {
            'user-agent': req.headers['user-agent'],
            'Content-Type': 'text/xml;charset=UTF-8',
            'soapAction': url,
        };


            const xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.wifi.ais.co.th\" xmlns:xsd=\"http://wifi.ais.co.th/xsd\">" +
                        "<soapenv:Header/>"+
                        "<soapenv:Body>"+
                            "<ws:registerAccount>"+
                                //  <!--Optional:-->
                                "<ws:acctInfo>"+
                                    // <!--Optional:-->
                                    "<xsd:captcha></xsd:captcha>"+
                                    // <!--Optional:-->
                                    "<xsd:country></xsd:country>"+
                                    // <!--Optional:-->
                                    "<xsd:dateOfBirth></xsd:dateOfBirth>"+
                                    // <!--Optional:-->
                                    "<xsd:emailAddress>" + regis_email + "</xsd:emailAddress>"+
                                    // <!--Optional:-->
                                    "<xsd:firstName>" + regis_firstname + "</xsd:firstName>"+
                                    // <!--Optional:-->
                                    "<xsd:idCardNo>" + regis_idcard + "</xsd:idCardNo>"+
                                    // <!--Optional:-->
                                    "<xsd:ipAddress>"+ipUse+"</xsd:ipAddress>"+
                                    // <!--Optional:-->
                                    "<xsd:isThai>1</xsd:isThai>"+
                                    // <!--Optional:-->
                                    "<xsd:lastName>" + regis_lastname + "</xsd:lastName>"+
                                    // <!--Optional:-->
                                    "<xsd:mobileNo>" + regis_mobile + "</xsd:mobileNo>"+
                                    // <!--Optional:-->
                                    "<xsd:partportNo>" + regis_passport + "</xsd:partportNo>"+
                                    // <!--Optional:-->
                                    "<xsd:projectCode>"+ constants.PROJECTCODE_PPCARD+"</xsd:projectCode>"+
                                    // <!--Optional:-->
                                    "<xsd:referenceId></xsd:referenceId>"+
                                    // <!--Optional:-->
                                    "<xsd:requestTime></xsd:requestTime>"+
                                    // <!--Optional:-->
                                    "<xsd:tid></xsd:tid>"+
                                    // <!--Optional:-->
                                    "<xsd:title></xsd:title>"+
                                    // <!--Optional:-->
                                    "<xsd:wifiPassword>"+ regis_password +"</xsd:wifiPassword>"+
                                    // <!--Optional:-->
                                    "<xsd:wifiUsername>"+ regis_username +"</xsd:wifiUsername>"+
                                "</ws:acctInfo>"+
                            "</ws:registerAccount>"+
                        "</soapenv:Body>"+
                        "</soapenv:Envelope>";
        console.log("xml>>>>>>"+xml);
        // usage of module
        const params = {
            EMAIL_ADDRESS: regis_email,
            FIRST_NAME: regis_firstname,
            LAST_NAME: regis_lastname,
            MOBILE_NO: regis_mobile,
            PASSPORT_ID: regis_passport,
            PORTAL_LANGUAGE: regis_language,
            THAI_CITIZEN_ID: regis_idcard,
            IPAddress: ipUse,
            username:regis_username,
            password:regis_password
        }

        console.log("underparam");
        var result = appcode.callsoapservice(url, xml, sampleHeaders).then(data => {
            console.log("data +" +JSON.stringify(data));
            const statusCode = data["statusCode"];
            console.log("statusCode +" + data["statusCode"]);
            parseString(data["body"], function (err, result) {
                if(err){
                     
                    const resData = {
                        isSuccess: false,
                        msg: systemConstant.MESSAGE('9999'),
                        responseCode: 9999,
                        ex: err
                    };    
                   
               
                    logger.registerlog_error('registerppcard', {
                     
                        clientIp: req.headers['x-forwarded-for'] || clientIp, 
                        statuscode: '500',
                        statusmessage: 'error',
                        params: params? JSON.stringify(params):'',                 
                        url: url,
                        response: JSON.stringify(resData),
                        resptime: new Date().getTime() - startDate,
                        ERROR:err.toString(),
                        UA: req.headers['user-agent'] || '',
                    });
            
                    
                    console.log(JSON.stringify((resData)));
                    res.json(JSON.stringify((resData)));
                
                }
                
                 var JOB = result["soapenv:Envelope"]["soapenv:Body"][0]["ns:registerAccountResponse"][0]["ns:return"][0];
                console.log("register account >>>>" + JSON.stringify(JOB));

                res.json(JSON.stringify(JOB));

                logger.registerlog('registerppcard', {

                    clientIp: req.headers['x-forwarded-for'] || clientIp, 
                    statuscode: statusCode,
                    statusmessage: 'ok',
                    params: JSON.stringify(params),                 
                    url: url,
                    response: JSON.stringify(JOB),
                    resptime: new Date().getTime() - startDate,
                    ERROR:'',
                    UA: req.headers['user-agent'] || '',
                });
            }
                 
            
            )

        },
        error =>{

                 
        const errjson = {
            responseCode: 8888,
            ex: error,                       
        }  
         console.log(JSON.stringify((errjson)));
   
        logger.registerlog_error('registerppcard', {
                     
            clientIp: req.headers['x-forwarded-for'] || clientIp, 
            statuscode: '500',
            statusmessage: 'error',
            params: params? JSON.stringify(params):'',                 
            url: url,
            response: JSON.stringify(errjson),
            resptime: new Date().getTime() - startDate,
            ERROR:error.toString(),
            UA: req.headers['user-agent'] || '',
        });


       
        res.json(JSON.stringify((errjson)));
        });


    } catch (ex) {

     
        const errjson = {
            responseCode: 9999,
            ex: ex,                       
        }  
         console.log(JSON.stringify((errjson)));
        
        // logger.error({
        //     "ACTION": "registerppcard",
        //     "ERR_MSG": ex.toString()
        // })

       
        res.json(JSON.stringify((errjson)));
    

    }
}


exports.CheckStatusLogon = function (req, res) {
    const ip = requestIp.getClientIp(req);
    const clientIp = ip.substring(ip.lastIndexOf(':') + 1, ip.length)
    const startDate = new Date().getTime();


    try {

        var ipUse = req.headers['x-forwarded-for'] || clientIp;

        // example data
        const url = constants.URLMCOA;
        console.log("url>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + url)

        const sampleHeaders = {
            'user-agent': req.headers['user-agent'],
            'Content-Type': 'text/xml;charset=UTF-8',
            'soapAction': url,
        };

        const xml = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:wifi=\"http://wifi.ais.co.th/\">" +
            "<soap:Header/>" +
            "<soap:Body>" +
            "<wifi:accountProfileStatusQuery>" +
            "<ipAddress>" + ipUse + "</ipAddress>" +
            "</wifi:accountProfileStatusQuery>" +
            "</soap:Body>" +
            "</soap:Envelope>";

        // usage of module

        var result = appcode.callsoapservice(url, xml, sampleHeaders).then(data => {

            const statusCode = data["statusCode"]

            console.log("CheckStatusLogon >>>>>>>>>>>>>xml>>>>>>>>>>>>>>>>>>>>> " + data["body"]);

            parseString(data["body"], function (err, result) {
                const params = {
                    ipAddress: ipUse
                }
                if(err){
                    
                    const errjson = {
                        response: 9999,
                        ex: err,                       
                    }
                    console.log(JSON.stringify((errjson)));
                    res.json(JSON.stringify((errjson)));
                
        
                    const resData = {
                        isSuccess: false,
                        msg: systemConstant.MESSAGE('9999'),
                        code: 9999,
                        ex: e
                    };
            

                    logger.log_error('accountProfileStatusQuery.', {
                                
                        clientIp: req.headers['x-forwarded-for'] || clientIp,                             
                        username:"",
                        pw : "-",                             
                        login_success :"" ,
                        replymessage : "",
                        responseCode : "",
                        responsemessage :"",
                        resptime: new Date().getTime() - startDate,
                        macAddress:"",
                        allowToAddMac:"",
                        unlimitedaccount : "",
                        timequata:"",
                        remainingTime:"",
                        sessiontime:"",
                        logonstatus:"",
                        speedprofile:"",
                        vlanId:"",
                        callingStationId:"",
                        url: url,
                        error:e,
                        UA: req.headers['user-agent'] || '',
                       
                    });
                }
                console.log("CheckStatusLogon >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>result" + result);
                var JOB = result["soap:Envelope"]["soap:Body"][0]["ns2:accountProfileStatusQueryResponse"][0]["return"][0];
                console.log("CheckStatusLogon >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>job" + JSON.stringify(JOB));
              
                res.json(JSON.stringify(JOB));

              
                logger.log('accountProfileStatusQuery', {
                                
                    clientIp: req.headers['x-forwarded-for'] || clientIp,                             
                    username:JOB.userName!=null ?JOB.userName :"" ,
                    pw :  "-",                             
                    login_success :JOB.login_success!=null ?JOB.login_success :"" ,
                    replymessage : JOB.replyMessage!=null ?JOB.replyMessage :"",
                    responseCode : JOB.responseCode!=null ?JOB.responseCode :"",
                    responsemessage : JOB.responseMessage!=null ?JOB.responseMessage :"",
                    resptime: new Date().getTime() - startDate,
                    macAddress:JOB.macAddress!=null ?JOB.macAddress :"",
                    allowToAddMac:JOB.allowToAddMac!=null ?JOB.allowToAddMac :"",
                    unlimitedaccount : JOB.unlimitedAccount!=null ?JOB.unlimitedAccount :"",
                    timequata:JOB.timeQuota!=null ?JOB.timeQuota :"",
                    remainingTime: JOB.remainingTime!=null ?JOB.remainingTime :"",
                    sessiontime:JOB.sessionTime!=null ?JOB.sessionTime :"",
                    logonstatus:JOB.logonStatus!=null ?JOB.logonStatus :"",
                    speedprofile:JOB.speedProfile!=null ?JOB.speedProfile :"",
                    vlanId:JOB.vlanId!=null ?JOB.vlanId :"",
                    callingStationId:JOB.callingStationId!=null ?JOB.callingStationId :"",
                    url: url,
                    error:"",
                    UA: req.headers['user-agent'] || '',
                   
                });


            });

        });


    } catch (ex) {
       
    
        
        logger.log_error('accountProfileStatusQuery.', {
                                
            clientIp: req.headers['x-forwarded-for'] || clientIp,                             
            username:"",
            password : "",                             
            login_success :"" ,
            replymessage : "",
            responseCode : "",
            responsemessage :"",
            resptime: new Date().getTime() - startDate,
            macAddress:"",
            allowToAddMac:"",
            unlimitedaccount : "",
            timequata:"",
            remainingTime:"",
            sessiontime:"",
            logonstatus:"",
            speedprofile:"",
            vlanId:"",
            callingStationId:"",
            url: url,
            error:ex.toString(),
            UA: req.headers['user-agent'] || '',
           
        });
        const errjson = {
            response: 9999,
            ex: ex,                       
        }
        console.log(JSON.stringify((errjson)));
        res.json(JSON.stringify((errjson)));
    

    }
}